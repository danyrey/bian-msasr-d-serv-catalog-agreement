/*
 * Copyright (c) 2023 Kairos
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @Descripción: Llave compuesta de la tabla bdisac:sac_convenios.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@Data


public class CatalogAgreementIndex implements Serializable {
	  /**
	   * Identificador para la serialización.
	   */
	  private static final long serialVersionUID = -5950009589382143756L;

	  /**
	   * Propiedad para el numero de categoria.
	   */
	  private String categoryNumber;
	  /**
	   * Propiedad para el numero de convenio.
	   */
	  private String agreementNumber;

}
