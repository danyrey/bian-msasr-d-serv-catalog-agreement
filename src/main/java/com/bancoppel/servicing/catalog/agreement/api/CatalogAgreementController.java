/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.api;

import com.bancoppel.commons.annotation.ValidateHeaders;
import com.bancoppel.servicing.catalog.agreement.constant.CatalogAgreementConstants;
import com.bancoppel.servicing.catalog.agreement.constant.ApiConstants;
import com.bancoppel.servicing.catalog.agreement.constant.Constants;
import com.bancoppel.servicing.catalog.agreement.constant.RoutesConstants;
import com.bancoppel.servicing.catalog.agreement.exceptions.ErrorResponse;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementRequest;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementResponse;
import com.bancoppel.servicing.catalog.agreement.service.CatalogAgreementService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

/**
 * 
 * @Descripción: Controlador de la clase.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@RestController
@Validated
@Slf4j
@RequestMapping(value = RoutesConstants.BASE_PATH)

public class CatalogAgreementController {

	/**
	 * Inyección de la dependencia {@link AgreementService}.
	 */
	@Autowired
	private CatalogAgreementService catalogAgreementService;

	/**
	 * Endpoint utilizado para la funcionalidad de consultar los convenios para
	 * pagos de servicio con ANTAD.
	 * 
	 * @param request Objeto de tipo {@link CatalogAgreementRequest}
	 * @return ResponseEntity body CatalogAgreementResponse object, status ok
	 */
	@ApiOperation(value = ApiConstants.OPERATION_API, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.DATE),
			@ApiImplicitParam(required = true, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.ACCEPT),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.ACCEPT_CHARSET),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.ACCEPT_ENCODING),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.ACCEPT_LANGUAGE),
			@ApiImplicitParam(required = true, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.AUTHORIZATION),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.HOST),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.USER_AGENT),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CHANNEL_ID),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CONTENT_ENCODING),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CONTENT_LANGUAGE),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CONTENT_LENGTH),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CONTENT_MD5),
			@ApiImplicitParam(required = true, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.CONTENT_TYPE),
			@ApiImplicitParam(required = true, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.UUID),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.DEVICE_ID),
			@ApiImplicitParam(required = false, paramType = Constants.HEADER_WORD_CONSTANT, name = ApiConstants.REFRESH_TOKEN) })
	@ApiResponses(value = {
			@ApiResponse(code = ApiConstants.CODE_OK, message = ApiConstants.OK, response = CatalogAgreementResponse.class),
			@ApiResponse(code = ApiConstants.CODE_BAD_REQUEST, message = ApiConstants.BAD_REQUEST, response = ErrorResponse.class),
			@ApiResponse(code = ApiConstants.CODE_RESOURCE_NOT_FOUND, message = ApiConstants.RESOURCE_NOT_FOUND, response = ErrorResponse.class),
			@ApiResponse(code = ApiConstants.CODE_REQUEST_TIME_OUT, message = ApiConstants.REQUEST_TIME_OUT, response = ErrorResponse.class),
			@ApiResponse(code = ApiConstants.CODE_INTERNAL_ERROR, message = ApiConstants.INTERNAL_ERROR, response = ErrorResponse.class) })

	@PostMapping(value = RoutesConstants.RETRIEVE_CATALOG_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ValidateHeaders
	public ResponseEntity<List<CatalogAgreementResponse>> getCatalogAgreement(
			@Valid @RequestBody CatalogAgreementRequest request) {
		log.debug(CatalogAgreementConstants.LOG_CATALOG_AGREEMENT_CONTROLLER);

		return new ResponseEntity<List<CatalogAgreementResponse>>(catalogAgreementService.getCatalogAgreement(request),
				HttpStatus.OK);
	}

}
