/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.entity;

import com.bancoppel.servicing.catalog.agreement.constant.InformixConstants;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * 
 * @Descripción: Entidad de la tabla sac_convenios de la base de datos bdisac.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@Data
@Entity
@Table(name = InformixConstants.TABLE_NAME_AGREEMENT, schema = InformixConstants.DATABASE_SHCEMA)
@IdClass(CatalogAgreementIndex.class)
public class CatalogAgreementEntity {
	  /**
	   * Propiedad para el numero de categoria.
	   */
	  @Id
	  @Column(name = InformixConstants.CATEGORY_NUMBER_FIELD)
	  private String categoryNumber;
	  /**
	   * Propiedad para el numero de convenio.
	   */
	  @Id
	  @Column(name = InformixConstants.AGREEMENT_NUMBER_FIELD)
	  private String agreementNumber;
	  /**
	   * Propiedad para la cuenta prestadora.
	   */
	  //@Column(name = InformixConstants.LENDING_ACCOUNT_FIELD)
	  //private String lendingAccount;
	  /**
	   * Propiedad para el campo trans_cen_cargo_cliente.
	   */
	  //@Column(name = InformixConstants.CUSTOMER_CARGO_FIELD)
	  //private String customerCargo;
	  /**
	   * Propiedad para el campo trans_cen_abono_convenio.
	   */
	  //@Column(name = InformixConstants.PAYMEN_AGREEMENT_FIELD)
	  //private String paymentAgreement;
	  /**
	   * Propiedad para el estatus del convenio.
	   */
	  //@Column(name = InformixConstants.AGREEMENT_STATUS_FIELD)
	  //private String agreementStatus;
	  /**
	   * Propiedad para el campo trans_suc_cargo.
	   */
	  //@Column(name = InformixConstants.BANK_BRANCH_CARGO_FIELD)
	  //private String bankBranchCargo;
	  /**
	   * Propiedad para el nombre del convenio.
	   */
	  @Column(name = InformixConstants.AGREEMENT_NAME_FIELD)
	  private String agreementName;
	  /**
     * Propiedad que representa el campo fechaapertura
     */
    @Column(name = InformixConstants.OPENING_DATE)
    private String openingDate;
    /**
     * Propiedad que representa el campo fechaclausura
     */
    @Column(name = InformixConstants.CLOSING_DATE)
    private String closingDate;
    /**
     * Propiedad que representa el campo fechaalta
     */
    @Column(name = InformixConstants.REGISTER_DATE)
    private String registerDate;
    /**
     * Propiedad que representa el campo statusconvenio
     */
    @Column(name = InformixConstants.STATUS_AGREEMENT)
    private String statusAgreement;
    /**
     * Propiedad que representa el campo tipo_referencia
     */
    @Column(name = InformixConstants.REFERENCE_TYPE)
    private String referenceType;
    /**
     * Propiedad que representa el campo nomlegalempresa
     */
    @Column(name = InformixConstants.LEGAL_AGREEMENT_NAME)
    private String legalAgreementName;
    /**
     * Propiedad que representa el campo rfcempresa
     */
    @Column(name = InformixConstants.AGREEMENT_RFC)
    private String agreementRfc;
    /**
     * Propiedad que representa el campo nomcomercialempresa
     */
    @Column(name = InformixConstants.BUSINESS_AGREEMENT_NAME)
    private String businessAgreementName;
    /**
     * Propiedad que representa el campo direccionempresa
     */
    @Column(name = InformixConstants.ADDRESS_AGREEMENT)
    private String addressAgreement;
    /**
     * Propiedad que representa el campo ciudad
     */
    @Column(name = InformixConstants.CITY)
    private String city;
    /**
     * Propiedad que representa el campo estado
     */
    @Column(name = InformixConstants.ESTATE)
    private String estate;
    /**
     * Propiedad que representa el campo codpostal
     */
    @Column(name = InformixConstants.ZIP_CODE)
    private String zipCode;
    /**
     * Propiedad que representa el campo departamento
     */
    @Column(name = InformixConstants.DEPARTMENT)
    private String department;
    /**
     * Propiedad que representa el campo numtelcorporativo
     */
    @Column(name = InformixConstants.CORPORATE_PHONE_NUMBER)
    private String corporatePhoneNumber;
    /**
     * Propiedad que representa el campo numfaxcorporativo
     */
    @Column(name = InformixConstants.CORPORATE_FAX_NUMBER)
    private String corporateFaxNumber;
    /**
     * Propiedad que representa el campo nomcontacto1
     */
    @Column(name = InformixConstants.CONTACT_NAME_1)
    private String contactName1;
    /**
     * Propiedad que representa el campo numtelcontacto1
     */
    @Column(name = InformixConstants.CONTACT_NUMBER_PHONE_1)
    private String contactNumberPhone1;
    /**
     * Propiedad que representa el campo numextcontacto1
     */
    @Column(name = InformixConstants.CONTACT_EXTENSION_NUMBER_1)
    private String contactExtensionNumber1;
    /**
     * Propiedad que representa el campo emailcontacto1
     */
    @Column(name = InformixConstants.CONTACT_EMAIL_1)
    private String contactEmail1;
    /**
     * Propiedad que representa el campo nomcontacto2
     */
    @Column(name = InformixConstants.CONTACT_NAME_2)
    private String contactName2;
    /**
     * Propiedad que representa el campo numtelcontacto2
     */
    @Column(name = InformixConstants.CONTACT_NUMBER_PHONE_2)
    private String contactNumberPhone2;
    /**
     * Propiedad que representa el campo numextcontacto2
     */
    @Column(name = InformixConstants.CONTACT_EXTENSION_NUMBER_2)
    private String contactExtensionNumber2;
    /**
     * Propiedad que representa el campo emailcontacto2
     */
    @Column(name = InformixConstants.CONTACT_EMAIL_2)
    private String contactEmail2;
    /**
     * Propiedad que representa el campo nomcontacto3
     */
    @Column(name = InformixConstants.CONTACT_NAME_3)
    private String contactName3;
    /**
     * Propiedad que representa el campo numtelcontacto3
     */
    @Column(name = InformixConstants.CONTACT_NUMBER_PHONE_3)
    private String contactNumberPhone3;
    /**
     * Propiedad que representa el campo numextcontacto3
     */
    @Column(name = InformixConstants.CONTACT_EXTENSION_NUMBER_3)
    private String contactExtensionNumber3;
    /**
     * Propiedad que representa el campo emailcontacto3
     */
    @Column(name = InformixConstants.CONTACT_EMAIL_3)
    private String contactEmail3;
    /**
     * Propiedad que representa el campo numcuentaclabe
     */
    @Column(name = InformixConstants.ACCOUNT_NUMBER_KEY)
    private String accountNumberKey;
    /**
     * Propiedad que representa el campo tipopago
     */
    @Column(name = InformixConstants.PAYMENT_TYPE )
    private String paymentType ;
    /**
     * Propiedad que representa el campo frecuenciapago
     */
    @Column(name = InformixConstants.PAYMENT_FREQUENCY)
    private String paymentFrequency;
    /**
     * Propiedad que representa el campo flgarchnotificacion
     */
    @Column(name = InformixConstants.NOTIFICATION_FILE_FLAG)
    private String notificationFileFlag;
    /**
     * Propiedad que representa el campo frecnotificacion
     */
    @Column(name = InformixConstants.NOTIFICATION_FREQUENCY)
    private String notificationFrequency;
    /**
     * Propiedad que representa el campo flgporccomtrans_conv
     */
    @Column(name = InformixConstants.TRANSACTION_FEE_AGREEMENT_FLAG)
    private String transactionFeeAgreementflag;
    /**
     * Propiedad que representa el campo porc_com_trans_conv
     */
    @Column(name = InformixConstants.TRANSACTION_FEE_AGREEMENT_VALUE)
    private String transactionFeeAgreementValue;
    /**
     * Propiedad que representa el campo flgporccomtotal_conv
     */
    @Column(name = InformixConstants.TOTAL_TRANSACTION_FEE_AGREEMENT_FLAG)
    private String totalTransactionFeeAgreementflag;
    /**
     * Propiedad que representa el campo porc_com_total_conv
     */
    @Column(name = InformixConstants.TOTAL_TRANSACTION_FEE_AGREEMENT_VALUE)
    private String totalTransactionFeeAgreementValue;
    /**
     * Propiedad que representa el campo flgimpcomtrans_conv
     */
    @Column(name = InformixConstants.TAX_COMISSION_AGREEMENT_FLAG)
    private String taxComissionAgreementflag;
    /**
     * Propiedad que representa el campo imp_com_trans_conv
     */
    @Column(name = InformixConstants.TAX_COMISSION_AGREEMENT_VAUE)
    private String taxComissionAgreementVaue;
    /**
     * Propiedad que representa el campo flgimpcomtotal_conv
     */
    @Column(name = InformixConstants.TOTAL_TAX_COMISSION_AGREEMENT_FLAG)
    private String totalTaxComissionAgreementflag;
    /**
     * Propiedad que representa el campo imp_com_total_conv
     */
    @Column(name = InformixConstants.TOTAL_TAX_COMISSION_AGREEMENT_VALUE)
    private String totalTaxComissionAgreementValue;
    /**
     * Propiedad que representa el campo flgivaincluido_conv
     */
    @Column(name = InformixConstants.VAT_INCLUDED_AGREEMENT_FLAG)
    private String vatIncludedAgreementFlag;
    /**
     * Propiedad que representa el campo iva_convenio
     */
    @Column(name = InformixConstants.VAT_AGREEMENT)
    private String vatAgreement;
    /**
     * Propiedad que representa el campo flgporccomtrans_cte
     */
    @Column(name = InformixConstants.TRANSACTION_FEE_CLIENT_FLAG)
    private String transactionFeeClientflag;
    /**
     * Propiedad que representa el campo porc_com_trans_cte
     */
    @Column(name = InformixConstants.TRANSACTION_FEE_CLIENT_VALUE)
    private String transactionFeeClientValue;
    /**
     * Propiedad que representa el campo flgimpcomtrans_cte
     */
    @Column(name = InformixConstants.VAT_INCLUDED_CLIENT_FLAG)
    private String vatIncludedClientFlag;
    /**
     * Propiedad que representa el campo imp_com_trans_cte
     */
    @Column(name = InformixConstants.VAT_CLIENT)
    private String vatClient;
    /**
     * Propiedad que representa el campo flg_ref1
     */
    @Column(name = InformixConstants.REFERENCE_FLAG_1)
    private String referenceFlag1;
    /**
     * Propiedad que representa el campo longitud_ref1
     */
    @Column(name = InformixConstants.LONGITUDE_REFERENCE_1)
    private String longitudeReference1;
    /**
     * Propiedad que representa el campo flgcalculodv_ref1
     */
    @Column(name = InformixConstants.CHECKER_DIGIT_CALCULATION_REFERENCE_FLAG_1)
    private String checkerDigitCalculationReferenceFlag1;
    /**
     * Propiedad que representa el campo nomrutinadv_ref1
     */
    @Column(name = InformixConstants.CHECKER_DIGIT_ROUTINE_NAME_REFERENCE_1)
    private String checkerDigitRoutineNameReference1;
    /**
     * Propiedad que representa el campo flg_ref2
     */
    @Column(name = InformixConstants.REFERENCE_FLAG_2)
    private String referenceFlag2;
    /**
     * Propiedad que representa el campo longitud_ref2
     */
    @Column(name = InformixConstants.LONGITUDE_REFERENCE_2)
    private String longitudeReference2;
    /**
     * Propiedad que representa el campo flgcalculodv_ref2
     */
    @Column(name = InformixConstants.CHECKER_DIGIT_CALCULATION_REFERENCE_FLAG_2)
    private String checkerDigitCalculationReferenceFlag2;
    /**
     * Propiedad que representa el campo nomrutinadv_ref2
     */
    @Column(name = InformixConstants.CHECKER_DIGIT_ROUTINE_NAME_REFERENCE_2)
    private String checkerDigitRoutineNameReference2;
    /**
     * Propiedad que representa el campo flgreporte
     */
    @Column(name = InformixConstants.REPORT_FLAG)
    private String reportFlag;
    /**
     * Propiedad que representa el campo nomreporte
     */
    @Column(name = InformixConstants.REPORT_NAME)
    private String reportName;
    /**
     * Propiedad que representa el campo cuenta_prestadora
     */
    @Column(name = InformixConstants.LENDER_ACCOUNT)
    private String lenderAccount;
    /**
     * Propiedad que representa el campo cuenta_contable
     */
    @Column(name = InformixConstants.LEDGER_ACCOUNT)
    private String ledgerAccount;
    /**
     * Propiedad que representa el campo trans_suc_cargo
     */
    @Column(name = InformixConstants.TRANSACTION_BRANCH_CHARGE)
    private String transactionBranchCharge;
    /**
     * Propiedad que representa el campo trans_suc_efectivo
     */
    @Column(name = InformixConstants.CASH_BRANCH_TRANSACTION)
    private String cashBranchTransaction;
    /**
     * Propiedad que representa el campo trans_cen_cargo_cliente
     */
    @Column(name = InformixConstants.CENTRAL_CUSTOMER_CHARGE_TRANSACTION)
    private String centralCustomerChargeTransaction;
    /**
     * Propiedad que representa el campo trans_cen_efectivo_cliente
     */
    @Column(name = InformixConstants.CUSTOMER_CASH_CENTRAL_TRANSACTION)
    private String customerCashCentralTransaction;
    /**
     * Propiedad que representa el campo trans_cen_abono_convenio
     */
    @Column(name = InformixConstants.CREDITOR_CASH_CENTRAL_TRANSACTION)
    private String creditorCashCentralTransaction;
    /**
     * Propiedad que representa el campo trans_cliq_cpl
     */
    @Column(name = InformixConstants.TRANSACTION_CLIQ_CPL)
    private String transactionCliqCpl;
    /**
     * Propiedad que representa el campo trans_aliq_cpl
     */
    @Column(name = InformixConstants.TRANSACTION_ALIQ_CPL)
    private String transactionAliqCpl;
    /**
     * Propiedad que representa el campo trans_cen_efectivo_cliente_cpl
     */
    @Column(name = InformixConstants.CENTRAL_TRANSACTION_CASH_CLIENT_CPL)
    private String centralTransactionCashClientCpl;
    /**
     * Propiedad que representa el campo nombre_referencia1
     */
    @Column(name = InformixConstants.REFERENCE_NAME_1)
    private String referenceName1;
    /**
     * Propiedad que representa el campo nombre_referencia2
     */
    @Column(name = InformixConstants.REFERENCE_NAME_2)
    private String referenceName2;
    /**
     * Propiedad que representa el campo nombre_archivo_cobranza
     */
    @Column(name = InformixConstants.COLLECTION_FILE_NAME)
    private String collectionFileName;
    /**
     * Propiedad que representa el campo ruta_archivo_cobranza
     */
    @Column(name = InformixConstants.COLLECTION_FILE_PATH)
    private String collectionFilePath;
    /**
     * Propiedad que representa el campo proceso_automatico
     */
    @Column(name = InformixConstants.AUTOMATIC_PROCESS)
    private String automaticProcess;
    /**
     * Propiedad que representa el campo fecha_ultimo_pago
     */
    @Column(name = InformixConstants.LAST_PAYMENT_DATE)
    private String lastPaymentDate;
    /**
     * Propiedad que representa el campo usuario_alta
     */
    @Column(name = InformixConstants.USER_REGISTER)
    private String userRegister;
    /**
     * Propiedad que representa el campo usuario_actualiza
     */
    @Column(name = InformixConstants.USER_UPDATE)
    private String userUpdate;
    /**
     * Propiedad que representa el campo fechaactualizacion
     */
    @Column(name = InformixConstants.UPDATE_DATE)
    private String updateDate;
    /**
     * Propiedad que representa el campo pagos_prog
     */
    @Column(name = InformixConstants.SCHEDULED_PAYMENTS)
    private String scheduledPayments;
    /**
     * Propiedad que representa el campo trans_cen_abono_cr
     */
    @Column(name = InformixConstants.CREDIT_CENTRAL_TRANSACTION_CR)
    private String creditCentralTransactionCr;
    /**
     * Propiedad que representa el campo trans_cen_abono_cf
     */
    @Column(name = InformixConstants.CREDIT_CENTRAL_TRANSACTION_CF)
    private String creditCentralTransactionCf;
    /**
     * Propiedad que representa el campo canal
     */
    @Column(name = InformixConstants.CHANNEL)
    private String channel;
    /**
     * Propiedad que representa el campo idemisor
     */
    @Column(name = InformixConstants.SENDER_ID)
    private String senderId;
    /**
     * Propiedad que representa el campo esquemacycdp
     */
    @Column(name = InformixConstants.CYCDP_SCHEME)
    private String cycdpScheme;
    /**
     * Propiedad que representa el campo consultaantad
     */
    @Column(name = InformixConstants.QUERY_AHEAD_ANTAD)
    private String queryAheadAntad;
    /**
     * Propiedad que representa el campo banderaimporte
     */
    @Column(name = InformixConstants.AMOUNT_FLAG)
    private String amountFlag;
    /**
     * Propiedad que representa el campo escaneocodigobarras
     */
    @Column(name = InformixConstants.BARCODE_SCANNING)
    private String barcodeScanning;
    /**
     * Propiedad que representa el campo banderaleyenda
     */
    @Column(name = InformixConstants.MESSAGE_FLAG)
    private String messageFlag;
    /**
     * Propiedad que representa el campo longitudminima
     */
    @Column(name = InformixConstants.MINIMUM_LENGTH)
    private String minimumLength;
    /**
     * Propiedad que representa el campo longitudmmaxima
     */
    @Column(name = InformixConstants.MAXIMUM_LENGTH)
    private String maximumLength;
    /**
     * Propiedad que representa el campo longitudtipodato
     */
    @Column(name = InformixConstants.DATA_TYPE_LENGTH)
    private String dataTypeLength;
    /**
     * Propiedad que representa el campo horarioinicio
     */
    @Column(name = InformixConstants.START_TIME)
    private String startTime;
    /**
     * Propiedad que representa el campo horariofin
     */
    @Column(name = InformixConstants.FINISH_TIME)
    private String finishTime;

}
