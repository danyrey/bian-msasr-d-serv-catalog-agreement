
/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.util;

import com.bancoppel.servicing.catalog.agreement.constant.Constants;
import com.bancoppel.servicing.catalog.agreement.entity.CatalogAgreementEntity;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

/**
 * Clase de utilerias generales.
 */
public class Util {

  /**
   * Variable para logeo.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);

  /**
   * Constructor privado para evitar la instancia de esta clase.
   */
  private Util() {

  }

  /**
   * Método para obtener un json String.
   * 
   * @param object.
   * @return Json Serializado.
   */
  public static String getJson(Object object) {
    String jsonString = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      jsonString = mapper.writeValueAsString(object);
    } catch (JsonProcessingException ex) {
      LOGGER.error(ex.getMessage(), ex);
    }

    return jsonString;
  }


  /**
   * Convierte un String a Map.
   * 
   * @param dataCacheStringJson.
   * @return Map.
   */
  public static Map<String, String> convertStringToDataCache(String dataCacheStringJson) {
    Map<String, String> dataCache = new HashMap<>();
    try {
      ObjectMapper mapper = new ObjectMapper();
      dataCache =
          mapper.readValue(dataCacheStringJson, new TypeReference<Map<String, String>>() {});
    } catch (IOException ex) {
      LOGGER.error(ex.getMessage(), ex);
    }
    return dataCache;
  }


  /**
   * Método para imprimir los headers de la petición Http.
   * 
   * @param request.
   */
  public static void printHeaders(HttpServletRequest request) {
    Enumeration<String> headerNames = request.getHeaderNames();

    if (!Objects.isNull(headerNames)) {

      while (headerNames.hasMoreElements()) {
        String key = headerNames.nextElement();
        String value = request.getHeader(key);
        LOGGER.debug(Constants.MSG_TO_LOG_HEADER, key, value);
      }
    }
  }

  /**
   * Método para 
   * 
   * @param request.
   */
  public static List<CatalogAgreementResponse> convertAll (List<CatalogAgreementEntity> listCatalogAgreementEntity){
	  //@SuppressWarnings("unchecked")
	  
	  List<CatalogAgreementResponse> listCatalogAgreementResponse = new ArrayList<CatalogAgreementResponse>();
	   
	  for (CatalogAgreementEntity catalogAgreementEntity : listCatalogAgreementEntity) {
		   CatalogAgreementResponse catalogAgreementResponse = new CatalogAgreementResponse();
		   catalogAgreementResponse.setAgreementNumber(catalogAgreementEntity.getAgreementNumber());
		   catalogAgreementResponse.setCategoryNumber(catalogAgreementEntity.getCategoryNumber());
		   catalogAgreementResponse.setOpeningDate(catalogAgreementEntity.getOpeningDate());
		   catalogAgreementResponse.setClosingDate(catalogAgreementEntity.getClosingDate());
		   catalogAgreementResponse.setRegisterDate(catalogAgreementEntity.getRegisterDate());
		   catalogAgreementResponse.setStatusAgreement(catalogAgreementEntity.getStatusAgreement());
		   catalogAgreementResponse.setReferenceType(catalogAgreementEntity.getReferenceType());
		   catalogAgreementResponse.setLegalAgreementName(catalogAgreementEntity.getLegalAgreementName());
		   catalogAgreementResponse.setAgreementRfc(catalogAgreementEntity.getAgreementRfc());
		   catalogAgreementResponse.setBusinessAgreementName(catalogAgreementEntity.getBusinessAgreementName().trim());
		   catalogAgreementResponse.setAddressAgreement(catalogAgreementEntity.getAddressAgreement());
		   catalogAgreementResponse.setCity(catalogAgreementEntity.getCity());
		   catalogAgreementResponse.setEstate(catalogAgreementEntity.getEstate());
		   catalogAgreementResponse.setZipCode(catalogAgreementEntity.getZipCode());
		   catalogAgreementResponse.setDepartment(catalogAgreementEntity.getDepartment());
		   catalogAgreementResponse.setCorporatePhoneNumber(catalogAgreementEntity.getCorporatePhoneNumber());
		   catalogAgreementResponse.setCorporateFaxNumber(catalogAgreementEntity.getCorporateFaxNumber());
		   catalogAgreementResponse.setContactName1(catalogAgreementEntity.getContactName1());
		   catalogAgreementResponse.setContactNumberPhone1(catalogAgreementEntity.getContactNumberPhone1());
		   catalogAgreementResponse.setContactExtensionNumber1(catalogAgreementEntity.getContactExtensionNumber1());
		   catalogAgreementResponse.setContactEmail1(catalogAgreementEntity.getContactEmail1());
		   catalogAgreementResponse.setContactName2(catalogAgreementEntity.getContactName2());
		   catalogAgreementResponse.setContactNumberPhone2(catalogAgreementEntity.getContactNumberPhone2());
		   catalogAgreementResponse.setContactExtensionNumber2(catalogAgreementEntity.getContactExtensionNumber2());
		   catalogAgreementResponse.setContactEmail2(catalogAgreementEntity.getContactEmail2());
		   catalogAgreementResponse.setContactName3(catalogAgreementEntity.getContactName3());
		   catalogAgreementResponse.setContactNumberPhone3(catalogAgreementEntity.getContactNumberPhone3());
		   catalogAgreementResponse.setContactExtensionNumber3(catalogAgreementEntity.getContactExtensionNumber3());
		   catalogAgreementResponse.setContactEmail3(catalogAgreementEntity.getContactEmail3());
		   catalogAgreementResponse.setAccountNumberKey(catalogAgreementEntity.getAccountNumberKey());
		   catalogAgreementResponse.setPaymentType(catalogAgreementEntity.getPaymentType());
		   catalogAgreementResponse.setPaymentFrequency(catalogAgreementEntity.getPaymentFrequency());
		   catalogAgreementResponse.setNotificationFileFlag(catalogAgreementEntity.getNotificationFileFlag());
		   catalogAgreementResponse.setNotificationFrequency(catalogAgreementEntity.getNotificationFrequency());
		   catalogAgreementResponse.setTransactionFeeAgreementflag(catalogAgreementEntity.getTransactionFeeAgreementflag());
		   catalogAgreementResponse.setTransactionFeeAgreementValue(catalogAgreementEntity.getTransactionFeeAgreementValue());
		   catalogAgreementResponse.setTotalTransactionFeeAgreementflag(catalogAgreementEntity.getTotalTransactionFeeAgreementflag());
		   catalogAgreementResponse.setTotalTransactionFeeAgreementValue(catalogAgreementEntity.getTotalTransactionFeeAgreementValue());
		   catalogAgreementResponse.setTaxComissionAgreementflag(catalogAgreementEntity.getTaxComissionAgreementflag());
		   catalogAgreementResponse.setTaxComissionAgreementVaue(catalogAgreementEntity.getTaxComissionAgreementVaue());
		   catalogAgreementResponse.setTotalTaxComissionAgreementflag(catalogAgreementEntity.getTotalTaxComissionAgreementflag());
		   catalogAgreementResponse.setTotalTaxComissionAgreementValue(catalogAgreementEntity.getTotalTaxComissionAgreementValue());
		   catalogAgreementResponse.setVatIncludedAgreementFlag(catalogAgreementEntity.getVatIncludedAgreementFlag());
		   catalogAgreementResponse.setVatAgreement(catalogAgreementEntity.getVatAgreement());
		   catalogAgreementResponse.setTransactionFeeClientflag(catalogAgreementEntity.getTransactionFeeClientflag());
		   catalogAgreementResponse.setTransactionFeeClientValue(catalogAgreementEntity.getTransactionFeeClientValue());
		   catalogAgreementResponse.setVatIncludedClientFlag(catalogAgreementEntity.getVatIncludedClientFlag());
		   catalogAgreementResponse.setVatClient(catalogAgreementEntity.getVatClient());
		   catalogAgreementResponse.setReferenceFlag1(catalogAgreementEntity.getReferenceFlag1());
		   catalogAgreementResponse.setLongitudeReference1(catalogAgreementEntity.getLongitudeReference1());
		   catalogAgreementResponse.setCheckerDigitCalculationReferenceFlag1(catalogAgreementEntity.getCheckerDigitCalculationReferenceFlag1());
		   catalogAgreementResponse.setCheckerDigitRoutineNameReference1(catalogAgreementEntity.getCheckerDigitRoutineNameReference1());
		   catalogAgreementResponse.setReferenceFlag2(catalogAgreementEntity.getReferenceFlag2());
		   catalogAgreementResponse.setLongitudeReference2(catalogAgreementEntity.getLongitudeReference2());
		   catalogAgreementResponse.setCheckerDigitCalculationReferenceFlag2(catalogAgreementEntity.getCheckerDigitCalculationReferenceFlag2());
		   catalogAgreementResponse.setCheckerDigitRoutineNameReference2(catalogAgreementEntity.getCheckerDigitRoutineNameReference2());
		   catalogAgreementResponse.setReportFlag(catalogAgreementEntity.getReportFlag());
		   catalogAgreementResponse.setReportName(catalogAgreementEntity.getReportName());
		   catalogAgreementResponse.setLenderAccount(catalogAgreementEntity.getLenderAccount());
		   catalogAgreementResponse.setLedgerAccount(catalogAgreementEntity.getLedgerAccount());
		   catalogAgreementResponse.setTransactionBranchCharge(catalogAgreementEntity.getTransactionBranchCharge());
		   catalogAgreementResponse.setCashBranchTransaction(catalogAgreementEntity.getCashBranchTransaction());
		   catalogAgreementResponse.setCentralCustomerChargeTransaction(catalogAgreementEntity.getCentralCustomerChargeTransaction());
		   catalogAgreementResponse.setCustomerCashCentralTransaction(catalogAgreementEntity.getCustomerCashCentralTransaction());
		   catalogAgreementResponse.setCreditorCashCentralTransaction(catalogAgreementEntity.getCreditorCashCentralTransaction());
		   catalogAgreementResponse.setTransactionCliqCpl(catalogAgreementEntity.getTransactionCliqCpl());
		   catalogAgreementResponse.setTransactionAliqCpl(catalogAgreementEntity.getTransactionAliqCpl());
		   catalogAgreementResponse.setCentralTransactionCashClientCpl(catalogAgreementEntity.getCentralTransactionCashClientCpl());
		   catalogAgreementResponse.setReferenceName1(catalogAgreementEntity.getReferenceName1());
		   catalogAgreementResponse.setReferenceName2(catalogAgreementEntity.getReferenceName2());
		   catalogAgreementResponse.setCollectionFileName(catalogAgreementEntity.getCollectionFileName());
		   catalogAgreementResponse.setCollectionFilePath(catalogAgreementEntity.getCollectionFilePath());
		   catalogAgreementResponse.setAutomaticProcess(catalogAgreementEntity.getAutomaticProcess());
		   catalogAgreementResponse.setLastPaymentDate(catalogAgreementEntity.getLastPaymentDate());
		   catalogAgreementResponse.setUserRegister(catalogAgreementEntity.getUserRegister());
		   catalogAgreementResponse.setUserUpdate(catalogAgreementEntity.getUserUpdate());
		   catalogAgreementResponse.setUpdateDate(catalogAgreementEntity.getUpdateDate());
		   catalogAgreementResponse.setScheduledPayments(catalogAgreementEntity.getScheduledPayments());
		   catalogAgreementResponse.setCreditCentralTransactionCr(catalogAgreementEntity.getCreditCentralTransactionCr());
		   catalogAgreementResponse.setCreditCentralTransactionCf(catalogAgreementEntity.getCreditCentralTransactionCf());
		   catalogAgreementResponse.setChannel(catalogAgreementEntity.getChannel());
		   catalogAgreementResponse.setSenderId(catalogAgreementEntity.getSenderId());
		   catalogAgreementResponse.setCycdpScheme(catalogAgreementEntity.getCycdpScheme());
		   catalogAgreementResponse.setQueryAheadAntad(catalogAgreementEntity.getQueryAheadAntad());
		   catalogAgreementResponse.setBarcodeScanning(catalogAgreementEntity.getBarcodeScanning());
		   catalogAgreementResponse.setMinimumLength(catalogAgreementEntity.getMinimumLength());
		   catalogAgreementResponse.setMaximumLength(catalogAgreementEntity.getMaximumLength());
		   catalogAgreementResponse.setDataTypeLength(catalogAgreementEntity.getDataTypeLength());
		   catalogAgreementResponse.setStartTime(catalogAgreementEntity.getStartTime());
		   catalogAgreementResponse.setFinishTime(catalogAgreementEntity.getFinishTime());
		   listCatalogAgreementResponse.add(catalogAgreementResponse);
	}
	 
	  return listCatalogAgreementResponse;
  }


}
