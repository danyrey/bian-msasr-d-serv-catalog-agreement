/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.constant;

/**
 * 
 * @Descripción: Constantes para informix.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
public class InformixConstants {
  /**
   * Constante que representa el nombre del esquema de informix bdisac.
   */
  public static final String DATABASE_SHCEMA = "bdisac:informix";
  /**
   * Constante que representa el nombre del esquema de informix para la base de datos bdibpi.
   */
  public static final String DATABASE_SCHEMA_BDIBPI = "bdibpi:informix";
  /**
   * Constante que representa el nombre de la tabla.
   */
  public static final String TABLE_NAME_AGREEMENT = "sac_convenios";
  /**
   * Constante que representa el campo cuenta_prestadora de la tabla bdisac:sac_convenios.
   */
  public static final String LENDING_ACCOUNT_FIELD = "cuenta_prestadora";
  /**
   * Constante que representa el campo trans_cen_cargo_cliente de la tabla bdisac:sac_convenios.
   */
  //public static final String CUSTOMER_CARGO_FIELD = "trans_cen_cargo_cliente";
  /**
   * Constante que representa el campo trans_cen_abono_convenio de la tabla bdisac:sac_convenios.
   */
  //public static final String PAYMEN_AGREEMENT_FIELD = "trans_cen_abono_convenio";
  /**
   * Constante que representa el campo statusconvenio de la tabla bdisac:sac_convenios.
   */
  //public static final String AGREEMENT_STATUS_FIELD = "statusconvenio";
  /**
   * Constante que representa el campo trans_suc_cargo de la tabla bdisac:sac_convenios.
   */
  //public static final String BANK_BRANCH_CARGO_FIELD = "trans_suc_cargo";
  /**
   * Constante que representa el campo numcategoria de la tabla bdisac:sac_convenios.
   */
  public static final String CATEGORY_NUMBER_FIELD = "numcategoria";
  /**
   * Constante que representa el campo numconvenio de la tabla sac_convenios.
   */
  public static final String AGREEMENT_NUMBER_FIELD = "numconvenio";
  /**
   * Constante que representa el campo nomconvenio de la tabla bdisac:sac_convenios.
   */
  public static final String AGREEMENT_NAME_FIELD = "nomconvenio";
  /**
   * Constante que representa el campo empresa.
   */
  public static final String COMPANY_FIELD = "empresa";
  /**
   * Constante que representa el nombre de la tabla bdisac: sac_param.
   */
  public static final String TABLE_NAME_PARAMETERS = "sac_param";
  /**
   * Constante que representa el campo cod_param de la tabla bdisac: sac_param.
   */
  public static final String PARAMETER_CODE_FIELD = "cod_param";
  /**
   * Constante que representa el campo valor de la tabla bdisac: sac_param.
   */
  public static final String VALUE_FIELD = "valor";
  /**
   * Constante que representa el nombre de la tabla bdibpi:bpi_cat_operaciones.
   */
  public static final String TABLE_NAME_CATALOG = "bpi_cat_operaciones";
  /**
   * Constante que representa el campo id_tran de la tabla bdibpi:bpi_cat_operaciones.
   */
  public static final String ID_TRANSACTION_FIELD = "id_tran";
  /**
   * Constante que representa el campo id_oper de la tabla bdibpi:bpi_cat_operaciones.
   */
  public static final String ID_OPERATION_FIELD = "id_oper";
  
  /**
   * Constante que representa el campo fechaapertura de la tabla bdisac:sac_convenios.
   */
  public static final String OPENING_DATE = "fechaapertura";
  /**
   * Constante que representa el campo fechaclausura de la tabla bdisac:sac_convenios.
   */
  public static final String CLOSING_DATE = "fechaclausura";
  /**
   * Constante que representa el campo fechaalta de la tabla bdisac:sac_convenios.
   */
  public static final String REGISTER_DATE = "fechaalta";
  /**
   * Constante que representa el campo statusconvenio de la tabla bdisac:sac_convenios.
   */
  public static final String STATUS_AGREEMENT = "statusconvenio";
  /**
   * Constante que representa el campo tipo_referencia de la tabla bdisac:sac_convenios.
   */
  public static final String REFERENCE_TYPE = "tipo_referencia";
  /**
   * Constante que representa el campo nomlegalempresa de la tabla bdisac:sac_convenios.
   */
  public static final String LEGAL_AGREEMENT_NAME = "nomlegalempresa";
  /**
   * Constante que representa el campo rfcempresa de la tabla bdisac:sac_convenios.
   */
  public static final String AGREEMENT_RFC = "rfcempresa";
  /**
   * Constante que representa el campo nomcomercialempresa de la tabla bdisac:sac_convenios.
   */
  public static final String BUSINESS_AGREEMENT_NAME = "nomcomercialempresa";
  /**
   * Constante que representa el campo direccionempresa de la tabla bdisac:sac_convenios.
   */
  public static final String ADDRESS_AGREEMENT = "direccionempresa";
  /**
   * Constante que representa el campo ciudad de la tabla bdisac:sac_convenios.
   */
  public static final String CITY = "ciudad";
  /**
   * Constante que representa el campo estado de la tabla bdisac:sac_convenios.
   */
  public static final String ESTATE = "estado";
  /**
   * Constante que representa el campo codpostal de la tabla bdisac:sac_convenios.
   */
  public static final String ZIP_CODE = "codpostal";
  /**
   * Constante que representa el campo departamento de la tabla bdisac:sac_convenios.
   */
  public static final String DEPARTMENT = "departamento";
  /**
   * Constante que representa el campo numtelcorporativo de la tabla bdisac:sac_convenios.
   */
  public static final String CORPORATE_PHONE_NUMBER = "numtelcorporativo";
  /**
   * Constante que representa el campo numfaxcorporativo de la tabla bdisac:sac_convenios.
   */
  public static final String CORPORATE_FAX_NUMBER = "numfaxcorporativo";
  /**
   * Constante que representa el campo nomcontacto1 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NAME_1 = "nomcontacto1";
  /**
   * Constante que representa el campo numtelcontacto1 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NUMBER_PHONE_1 = "numtelcontacto1";
  /**
   * Constante que representa el campo numextcontacto1 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EXTENSION_NUMBER_1 = "numextcontacto1";
  /**
   * Constante que representa el campo emailcontacto1 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EMAIL_1 = "emailcontacto1";
  /**
   * Constante que representa el campo nomcontacto2 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NAME_2 = "nomcontacto2";
  /**
   * Constante que representa el campo numtelcontacto2 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NUMBER_PHONE_2 = "numtelcontacto2";
  /**
   * Constante que representa el campo numextcontacto2 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EXTENSION_NUMBER_2 = "numextcontacto2";
  /**
   * Constante que representa el campo emailcontacto2 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EMAIL_2 = "emailcontacto2";
  /**
   * Constante que representa el campo nomcontacto3 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NAME_3 = "nomcontacto3";
  /**
   * Constante que representa el campo numtelcontacto3 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_NUMBER_PHONE_3 = "numtelcontacto3";
  /**
   * Constante que representa el campo numextcontacto3 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EXTENSION_NUMBER_3 = "numextcontacto3";
  /**
   * Constante que representa el campo emailcontacto3 de la tabla bdisac:sac_convenios.
   */
  public static final String CONTACT_EMAIL_3 = "emailcontacto3";
  /**
   * Constante que representa el campo numcuentaclabe de la tabla bdisac:sac_convenios.
   */
  public static final String ACCOUNT_NUMBER_KEY = "numcuentaclabe";
  /**
   * Constante que representa el campo tipopago de la tabla bdisac:sac_convenios.
   */
  public static final String PAYMENT_TYPE  = "tipopago";
  /**
   * Constante que representa el campo frecuenciapago de la tabla bdisac:sac_convenios.
   */
  public static final String PAYMENT_FREQUENCY = "frecuenciapago";
  /**
   * Constante que representa el campo flgarchnotificacion de la tabla bdisac:sac_convenios.
   */
  public static final String NOTIFICATION_FILE_FLAG = "flgarchnotificacion";
  /**
   * Constante que representa el campo frecnotificacion de la tabla bdisac:sac_convenios.
   */
  public static final String NOTIFICATION_FREQUENCY = "frecnotificacion";
  /**
   * Constante que representa el campo flgporccomtrans_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_FEE_AGREEMENT_FLAG = "flgporccomtrans_conv";
  /**
   * Constante que representa el campo porc_com_trans_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_FEE_AGREEMENT_VALUE = "porc_com_trans_conv";
  /**
   * Constante que representa el campo flgporccomtotal_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TOTAL_TRANSACTION_FEE_AGREEMENT_FLAG = "flgporccomtotal_conv";
  /**
   * Constante que representa el campo porc_com_total_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TOTAL_TRANSACTION_FEE_AGREEMENT_VALUE = "porc_com_total_conv";
  /**
   * Constante que representa el campo flgimpcomtrans_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TAX_COMISSION_AGREEMENT_FLAG = "flgimpcomtrans_conv";
  /**
   * Constante que representa el campo imp_com_trans_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TAX_COMISSION_AGREEMENT_VAUE = "imp_com_trans_conv";
  /**
   * Constante que representa el campo flgimpcomtotal_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TOTAL_TAX_COMISSION_AGREEMENT_FLAG = "flgimpcomtotal_conv";
  /**
   * Constante que representa el campo imp_com_total_conv de la tabla bdisac:sac_convenios.
   */
  public static final String TOTAL_TAX_COMISSION_AGREEMENT_VALUE = "imp_com_total_conv";
  /**
   * Constante que representa el campo flgivaincluido_conv de la tabla bdisac:sac_convenios.
   */
  public static final String VAT_INCLUDED_AGREEMENT_FLAG = "flgivaincluido_conv";
  /**
   * Constante que representa el campo iva_convenio de la tabla bdisac:sac_convenios.
   */
  public static final String VAT_AGREEMENT = "iva_convenio";
  /**
   * Constante que representa el campo flgporccomtrans_cte de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_FEE_CLIENT_FLAG = "flgporccomtrans_cte";
  /**
   * Constante que representa el campo porc_com_trans_cte de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_FEE_CLIENT_VALUE = "porc_com_trans_cte";
  /**
   * Constante que representa el campo flgimpcomtrans_cte de la tabla bdisac:sac_convenios.
   */
  public static final String VAT_INCLUDED_CLIENT_FLAG = "flgimpcomtrans_cte";
  /**
   * Constante que representa el campo imp_com_trans_cte de la tabla bdisac:sac_convenios.
   */
  public static final String VAT_CLIENT = "imp_com_trans_cte";
  /**
   * Constante que representa el campo flg_ref1 de la tabla bdisac:sac_convenios.
   */
  public static final String REFERENCE_FLAG_1 = "flg_ref1";
  /**
   * Constante que representa el campo longitud_ref1 de la tabla bdisac:sac_convenios.
   */
  public static final String LONGITUDE_REFERENCE_1 = "longitud_ref1";
  /**
   * Constante que representa el campo flgcalculodv_ref1 de la tabla bdisac:sac_convenios.
   */
  public static final String CHECKER_DIGIT_CALCULATION_REFERENCE_FLAG_1 = "flgcalculodv_ref1";
  /**
   * Constante que representa el campo nomrutinadv_ref1 de la tabla bdisac:sac_convenios.
   */
  public static final String CHECKER_DIGIT_ROUTINE_NAME_REFERENCE_1 = "nomrutinadv_ref1";
  /**
   * Constante que representa el campo flg_ref2 de la tabla bdisac:sac_convenios.
   */
  public static final String REFERENCE_FLAG_2 = "flg_ref2";
  /**
   * Constante que representa el campo longitud_ref2 de la tabla bdisac:sac_convenios.
   */
  public static final String LONGITUDE_REFERENCE_2 = "longitud_ref2";
  /**
   * Constante que representa el campo flgcalculodv_ref2 de la tabla bdisac:sac_convenios.
   */
  public static final String CHECKER_DIGIT_CALCULATION_REFERENCE_FLAG_2 = "flgcalculodv_ref2";
  /**
   * Constante que representa el campo nomrutinadv_ref2 de la tabla bdisac:sac_convenios.
   */
  public static final String CHECKER_DIGIT_ROUTINE_NAME_REFERENCE_2 = "nomrutinadv_ref2";
  /**
   * Constante que representa el campo flgreporte de la tabla bdisac:sac_convenios.
   */
  public static final String REPORT_FLAG = "flgreporte";
  /**
   * Constante que representa el campo nomreporte de la tabla bdisac:sac_convenios.
   */
  public static final String REPORT_NAME = "nomreporte";
  /**
   * Constante que representa el campo cuenta_prestadora de la tabla bdisac:sac_convenios.
   */
  public static final String LENDER_ACCOUNT = "cuenta_prestadora";
  /**
   * Constante que representa el campo cuenta_contable de la tabla bdisac:sac_convenios.
   */
  public static final String LEDGER_ACCOUNT = "cuenta_contable";
  /**
   * Constante que representa el campo trans_suc_cargo de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_BRANCH_CHARGE = "trans_suc_cargo";
  /**
   * Constante que representa el campo trans_suc_efectivo de la tabla bdisac:sac_convenios.
   */
  public static final String CASH_BRANCH_TRANSACTION = "trans_suc_efectivo";
  /**
   * Constante que representa el campo trans_cen_cargo_cliente de la tabla bdisac:sac_convenios.
   */
  public static final String CENTRAL_CUSTOMER_CHARGE_TRANSACTION = "trans_cen_cargo_cliente";
  /**
   * Constante que representa el campo trans_cen_efectivo_cliente de la tabla bdisac:sac_convenios.
   */
  public static final String CUSTOMER_CASH_CENTRAL_TRANSACTION = "trans_cen_efectivo_cliente";
  /**
   * Constante que representa el campo trans_cen_abono_convenio de la tabla bdisac:sac_convenios.
   */
  public static final String CREDITOR_CASH_CENTRAL_TRANSACTION = "trans_cen_abono_convenio";
  /**
   * Constante que representa el campo trans_cliq_cpl de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_CLIQ_CPL = "trans_cliq_cpl";
  /**
   * Constante que representa el campo trans_aliq_cpl de la tabla bdisac:sac_convenios.
   */
  public static final String TRANSACTION_ALIQ_CPL = "trans_aliq_cpl";
  /**
   * Constante que representa el campo trans_cen_efectivo_cliente_cpl de la tabla bdisac:sac_convenios.
   */
  public static final String CENTRAL_TRANSACTION_CASH_CLIENT_CPL = "trans_cen_efectivo_cliente_cpl";
  /**
   * Constante que representa el campo nombre_referencia1 de la tabla bdisac:sac_convenios.
   */
  public static final String REFERENCE_NAME_1 = "nombre_referencia1";
  /**
   * Constante que representa el campo nombre_referencia2 de la tabla bdisac:sac_convenios.
   */
  public static final String REFERENCE_NAME_2 = "nombre_referencia2";
  /**
   * Constante que representa el campo nombre_archivo_cobranza de la tabla bdisac:sac_convenios.
   */
  public static final String COLLECTION_FILE_NAME = "nombre_archivo_cobranza";
  /**
   * Constante que representa el campo ruta_archivo_cobranza de la tabla bdisac:sac_convenios.
   */
  public static final String COLLECTION_FILE_PATH = "ruta_archivo_cobranza";
  /**
   * Constante que representa el campo proceso_automatico de la tabla bdisac:sac_convenios.
   */
  public static final String AUTOMATIC_PROCESS = "proceso_automatico";
  /**
   * Constante que representa el campo fecha_ultimo_pago de la tabla bdisac:sac_convenios.
   */
  public static final String LAST_PAYMENT_DATE = "fecha_ultimo_pago";
  /**
   * Constante que representa el campo usuario_alta de la tabla bdisac:sac_convenios.
   */
  public static final String USER_REGISTER = "usuario_alta";
  /**
   * Constante que representa el campo usuario_actualiza de la tabla bdisac:sac_convenios.
   */
  public static final String USER_UPDATE = "usuario_actualiza";
  /**
   * Constante que representa el campo fechaactualizacion de la tabla bdisac:sac_convenios.
   */
  public static final String UPDATE_DATE = "fechaactualizacion";
  /**
   * Constante que representa el campo pagos_prog de la tabla bdisac:sac_convenios.
   */
  public static final String SCHEDULED_PAYMENTS = "pagos_prog";
  /**
   * Constante que representa el campo trans_cen_abono_cr de la tabla bdisac:sac_convenios.
   */
  public static final String CREDIT_CENTRAL_TRANSACTION_CR = "trans_cen_abono_cr";
  /**
   * Constante que representa el campo trans_cen_abono_cf de la tabla bdisac:sac_convenios.
   */
  public static final String CREDIT_CENTRAL_TRANSACTION_CF = "trans_cen_abono_cf";
  /**
   * Constante que representa el campo canal de la tabla bdisac:sac_convenios.
   */
  public static final String CHANNEL = "canal";
  /**
   * Constante que representa el campo idemisor de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String SENDER_ID = "idemisor";
  /**
   * Constante que representa el campo esquemacycdp de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String CYCDP_SCHEME = "esquemacycdp";
  /**
   * Constante que representa el campo consultaantad de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String QUERY_AHEAD_ANTAD = "consultaantad";
  /**
   * Constante que representa el campo banderaimporte de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String AMOUNT_FLAG = "banderaimporte";
  /**
   * Constante que representa el campo escaneocodigobarras de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String BARCODE_SCANNING = "escaneocodigobarras";
  /**
   * Constante que representa el campo banderaleyenda de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String MESSAGE_FLAG = "banderaleyenda";
  /**
   * Constante que representa el campo longitudminima de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String MINIMUM_LENGTH = "longitudminima";
  /**
   * Constante que representa el campo longitudmmaxima de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String MAXIMUM_LENGTH = "longitudmmaxima";
  /**
   * Constante que representa el campo longitudtipodato de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String DATA_TYPE_LENGTH = "longitudtipodato";
  /**
   * Constante que representa el campo horarioinicio de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String START_TIME = "horarioinicio";
  /**
   * Constante que representa el campo horariofin de la tabla bdisac:sac_convenios_antad_det.
   */
  public static final String FINISH_TIME = "horariofin";
  
  /**
   * Private constructor will prevent the instantiation of this class.
   */
  private InformixConstants() {}

}
