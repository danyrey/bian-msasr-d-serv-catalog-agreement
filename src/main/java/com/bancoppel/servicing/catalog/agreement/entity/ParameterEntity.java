/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.entity;

import com.bancoppel.servicing.catalog.agreement.constant.InformixConstants;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @Descripción: Entidad para la tabla bdisac:sac_param.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@Data
@Entity
@Table(name = InformixConstants.TABLE_NAME_PARAMETERS, schema = InformixConstants.DATABASE_SHCEMA)
public class ParameterEntity {

  /**
   * Propiedad que representa el codigo del parametro.
   */
  @Id
  @Column(name = InformixConstants.PARAMETER_CODE_FIELD)
  private String parameterCode;
  /**
   * Propiedad que representa el valor del parametro.
   */
  @Column(name = InformixConstants.VALUE_FIELD)
  private String value;

}
