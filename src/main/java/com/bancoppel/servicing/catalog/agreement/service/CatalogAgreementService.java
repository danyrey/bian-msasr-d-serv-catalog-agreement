/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.service;

import java.util.List;

import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementRequest;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementResponse;

/**
 * 
 * @Descripción: Define los metodos para obtener los convenios de los pagos de servicios con ANTAD.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
public interface CatalogAgreementService {
	  List<CatalogAgreementResponse> getCatalogAgreement(CatalogAgreementRequest request);

}
