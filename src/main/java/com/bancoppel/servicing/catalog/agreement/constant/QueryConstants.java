/*
 * Copyright (c) 2019 Bancoppel
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.constant;

/**
 * 
 * @Descripción: Define los metodos para obtener los convenios de los pagos de servicios con ANTAD.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
public class QueryConstants {

  /**
   * Consulta para obtener el convenio.
   */
  public static final String RETRIEVE_AGREEMENT_LIST =
      "SELECT  "
		  + "TRIM(convenios.numcategoria) AS numcategoria, TRIM(convenios.numconvenio) AS numconvenio, TRIM(convenios.nomconvenio) AS nomconvenio, convenios.fechaapertura, "
		  + "convenios.fechaclausura, convenios.fechaalta, TRIM(convenios.statusconvenio) AS statusconvenio, TRIM(convenios.tipo_referencia) AS tipo_referencia, "
		  + "TRIM(convenios.nomlegalempresa) AS nomlegalempresa, TRIM(convenios.rfcempresa) AS rfcempresa, TRIM(convenios.nomcomercialempresa) AS nomcomercialempresa, TRIM(convenios.direccionempresa) AS direccionempresa, "
		  + "TRIM(convenios.ciudad) AS ciudad, TRIM(convenios.estado) AS estado, TRIM(convenios.codpostal) AS codpostal, TRIM(convenios.departamento) AS departamento, "
		  + "TRIM(convenios.numtelcorporativo) AS numtelcorporativo, TRIM(convenios.numfaxcorporativo) AS numfaxcorporativo, TRIM(convenios.nomcontacto1) AS nomcontacto1, TRIM(convenios.numtelcontacto1) AS numtelcontacto1, "
		  + "TRIM(convenios.numextcontacto1) AS numextcontacto1, TRIM(convenios.emailcontacto1) AS emailcontacto1, TRIM(convenios.nomcontacto2) AS nomcontacto2, TRIM(convenios.numtelcontacto2) AS numtelcontacto2, "
		  + "TRIM(convenios.numextcontacto2) AS numextcontacto2, TRIM(convenios.emailcontacto2) AS emailcontacto2, TRIM(convenios.nomcontacto3) AS nomcontacto3, TRIM(convenios.numtelcontacto3) AS numtelcontacto3, "
		  + "TRIM(convenios.numextcontacto3) AS numextcontacto3, TRIM(convenios.emailcontacto3) AS emailcontacto3, TRIM(convenios.numcuentaclabe) AS numcuentaclabe, TRIM(convenios.tipopago) AS tipopago, "
		  + "convenios.frecuenciapago, TRIM(convenios.flgarchnotificacion) AS flgarchnotificacion, convenios.frecnotificacion, "
		  + "TRIM(convenios.flgporccomtrans_conv) AS flgporccomtrans_conv, convenios.porc_com_trans_conv, TRIM(convenios.flgporccomtotal_conv) AS flgporccomtotal_conv, "
		  + "convenios.porc_com_total_conv, TRIM(convenios.flgimpcomtrans_conv) AS flgimpcomtrans_conv, convenios.imp_com_trans_conv, "
		  + "TRIM(convenios.flgimpcomtotal_conv) AS flgimpcomtotal_conv, convenios.imp_com_total_conv, TRIM(convenios.flgivaincluido_conv) AS flgivaincluido_conv, "
		  + "convenios.iva_convenio, TRIM(convenios.flgporccomtrans_cte) AS flgporccomtrans_cte, convenios.porc_com_trans_cte, "
		  + "TRIM(convenios.flgimpcomtrans_cte) AS flgimpcomtrans_cte, convenios.imp_com_trans_cte, TRIM(convenios.flg_ref1) AS flg_ref1, convenios.longitud_ref1, "
		  + "TRIM(convenios.flgcalculodv_ref1) AS flgcalculodv_ref1, TRIM(convenios.nomrutinadv_ref1) AS nomrutinadv_ref1, TRIM(convenios.flg_ref2) AS flg_ref2, convenios.longitud_ref2, "
		  + "TRIM(convenios.flgcalculodv_ref2) AS flgcalculodv_ref2, TRIM(convenios.nomrutinadv_ref2) AS nomrutinadv_ref2, TRIM(convenios.flgreporte) AS flgreporte, TRIM(convenios.nomreporte) AS nomreporte, "
		  + "TRIM(convenios.cuenta_prestadora) AS cuenta_prestadora, TRIM(convenios.cuenta_contable) AS cuenta_contable, TRIM(convenios.trans_suc_cargo) AS trans_suc_cargo, "
		  + "TRIM(convenios.trans_suc_efectivo) AS trans_suc_efectivo, TRIM(convenios.trans_cen_cargo_cliente) AS trans_cen_cargo_cliente, TRIM(convenios.trans_cen_efectivo_cliente) AS trans_cen_efectivo_cliente, "
		  + "TRIM(convenios.trans_cen_abono_convenio) AS trans_cen_abono_convenio, TRIM(convenios.trans_cliq_cpl) AS trans_cliq_cpl, TRIM(convenios.trans_aliq_cpl) AS trans_aliq_cpl, "
		  + "TRIM(convenios.trans_cen_efectivo_cliente_cpl) AS trans_cen_efectivo_cliente_cpl, TRIM(convenios.nombre_referencia1) nombre_referencia1, TRIM(convenios.nombre_referencia2) AS nombre_referencia2, "
		  + "TRIM(convenios.nombre_archivo_cobranza) AS nombre_archivo_cobranza, TRIM(convenios.ruta_archivo_cobranza) AS ruta_archivo_cobranza, convenios.proceso_automatico, "
		  + "convenios.fecha_ultimo_pago, TRIM(convenios.usuario_alta) AS usuario_alta, TRIM(convenios.usuario_actualiza) AS usuario_actualiza, "
		  + "convenios.fechaactualizacion, convenios.pagos_prog, TRIM(convenios.trans_cen_abono_cr) AS trans_cen_abono_cr, "
		  + "TRIM(convenios.trans_cen_abono_cf) AS trans_cen_abono_cf, TRIM(convenios.canal) AS canal, TRIM(convenios_antad_det.idemisor) AS idemisor, TRIM(convenios_antad_det.esquemacycdp) AS esquemacycdp, "
		  + "TRIM(convenios_antad_det.consultaantad) AS consultaantad, TRIM(convenios_antad_det.banderaimporte) AS banderaimporte, TRIM(convenios_antad_det.escaneocodigobarras) AS escaneocodigobarras, "
		  + "TRIM(convenios_antad_det.banderaleyenda) AS banderaleyenda, TRIM(convenios_antad_det.longitudminima) AS longitudminima, TRIM(convenios_antad_det.longitudmmaxima) AS longitudmmaxima, "
		  + "TRIM(convenios_antad_det.longitudtipodato) AS longitudtipodato, TRIM(convenios_antad_det.horarioinicio) AS horarioinicio, TRIM(convenios_antad_det.horariofin) AS horariofin "
          + "FROM bdisac\\:informix.sac_convenios convenios "
          + "INNER JOIN  bdisac\\:informix.sac_convenios_antad_det as convenios_antad_det  "
          + "ON convenios_antad_det.numconvenio = convenios.numconvenio and convenios_antad_det.numcategoria = convenios.numcategoria "
          + "WHERE convenios.numcategoria = :categoryNumber "
          + "AND convenios.statusconvenio = :statusconvenio";
  /**
   * Campo numero de categoria.
   */
  public static final String CATEGORY_NUMBER_FIELD = "categoryNumber";
  /**
   * Campo estatus del convenio.
   */
  public static final String STATUS = "statusconvenio";

  /**
   * Constructor de la clase.
   */
  private QueryConstants() {}
}
