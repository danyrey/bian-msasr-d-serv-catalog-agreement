package com.bancoppel.servicing.catalog.agreement.constant;

public class RoutesConstants {
	  
	  /**
	   * Base path del ms.
	   */
	  public static final String BASE_PATH = "${api.basePath}";
	  /**
	   * Constante para el path.
	   */
	  public static final String RETRIEVE_CATALOG_PATH = "${api.catalog.mapping}";
	  
	  /**
	   * Constructor privado para evitar la instancia de esta clase.
	   */
	  private RoutesConstants() {
	    
	  }

}
