/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.model;

import io.swagger.annotations.ApiModel;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 
 * @Descripción: POJO que define la petición al servicio.
 * @Autor: Miguel López
 * @Fecha: Abr 21, 2023
 * @Empresa: Kairos DS
 */

@ApiModel
@Data
public class CatalogAgreementRequest {
	  /**
	   * Propiedad para el numero de categoria del servicio.
	   */
	  @NotEmpty
	  private String categoryNumber;
	  /**
	   * Propiedad para el estatus de la categoria del servicio.
	   */
	  private String status;
	  

}
