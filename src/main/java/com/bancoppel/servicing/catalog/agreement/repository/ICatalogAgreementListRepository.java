/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.repository;

import com.bancoppel.commons.annotation.HandledProcedure;
import com.bancoppel.servicing.catalog.agreement.constant.CatalogAgreementConstants;
import com.bancoppel.servicing.catalog.agreement.constant.QueryConstants;
import com.bancoppel.servicing.catalog.agreement.entity.CatalogAgreementEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 
 * @Descripción: Repositorio para los convenios de pagos de servicio.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@Repository
public interface ICatalogAgreementListRepository extends JpaRepository<CatalogAgreementEntity, String>  {
	  /**
	   * Consulta los convenios para los pagos de servicio con ANTAD.
	   * 
	   * @param categoryNumber numero de categoria del servicio.
	   * @param statusconvenio numero de convenio del servicio.
	   * @return Objeto de tipo {@link CatalogAgreementEntity}
	   */
	  @HandledProcedure(value = CatalogAgreementConstants.TIME_REPOSITORY_AGREEMENT,
	      name = CatalogAgreementConstants.TIME_REPOSITORY_AGREEMENT)
	  @Query(value = QueryConstants.RETRIEVE_AGREEMENT_LIST, nativeQuery = true)
	public List<CatalogAgreementEntity> findByCategoryNumberAndStatus(
	      @Param(QueryConstants.CATEGORY_NUMBER_FIELD) String categoryNumber,
	      @Param(QueryConstants.STATUS) String statusconvenio);

}
