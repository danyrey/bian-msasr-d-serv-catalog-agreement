/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.business;

import com.bancoppel.commons.annotation.HandledProcedure;
import com.bancoppel.commons.exceptions.ExternalResourceException;
import com.bancoppel.servicing.catalog.agreement.constant.CatalogAgreementConstants;
import com.bancoppel.servicing.catalog.agreement.entity.CatalogAgreementEntity;
import com.bancoppel.servicing.catalog.agreement.exceptions.custom.DatabaseTimeoutException;
import com.bancoppel.servicing.catalog.agreement.exceptions.custom.NotFoundAgreementException;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementRequest;
import com.bancoppel.servicing.catalog.agreement.model.CatalogAgreementResponse;
import com.bancoppel.servicing.catalog.agreement.repository.ICatalogAgreementListRepository;
import com.bancoppel.servicing.catalog.agreement.service.CatalogAgreementService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.netflix.hystrix.exception.HystrixTimeoutException;
import com.bancoppel.servicing.catalog.agreement.util.Util;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @description: Implementa la logica del servicio {@link CatalogAgreementService}.
 * @Autor: Miguel López
 * @Fecha: Abr 20, 2023
 * @Empresa: Kairos DS
 */
@Slf4j
@Service
public class CatalogAgreementBusiness implements CatalogAgreementService {
	  /**
	   * Inyección del repositorio {@link ICatalogAgreementRepository}.
	   */
	  @Autowired
	  private ICatalogAgreementListRepository agreementRepository;

	  /**
	   * {@inheritDoc}.
	   */
	@Override
	  @HystrixCommand(fallbackMethod = CatalogAgreementConstants.AGREEMENT_FALLBACK,
	      ignoreExceptions = {NotFoundAgreementException.class})
	  @HandledProcedure(name = CatalogAgreementConstants.PAYMENT_AGREEMENT_BUSINESS_TIME,
	      value = CatalogAgreementConstants.PAYMENT_AGREEMENT_BUSINESS_TIME,
	      ignoreExceptions = NotFoundAgreementException.class)
	  public List<CatalogAgreementResponse> getCatalogAgreement(CatalogAgreementRequest request) {
	    log.debug(" ** Request: {}", request); 

	    List<CatalogAgreementResponse> listAgreementResponse = null;

	    List<CatalogAgreementEntity> listCatalogAgreementEntity =
	        Optional
	            .ofNullable(this.agreementRepository.findByCategoryNumberAndStatus(
	                request.getCategoryNumber(), request.getStatus()))
	            .orElseThrow(
	                () -> new NotFoundAgreementException(CatalogAgreementConstants.MSG_AGREEMENT_NOT_FOUNT));



	    listAgreementResponse = new ArrayList<CatalogAgreementResponse>();
	    
	    listAgreementResponse = Util.convertAll(listCatalogAgreementEntity);
	    
	    log.debug(" ** Response: {}", listAgreementResponse.toString());
	    
	    
	    
	    return listAgreementResponse;
	  }

	  /**
	   * Fallback de hystrix.
	   * 
	   * @param request Objeto de tipo {@link CatalogAgreementRequest}
	   * @return una excepcion de tipo DatabaseTimeoutException o ExternalResourceException
	   * @throws DatabaseTimeoutException si ocurrio un error por timeout o
	   *         {@link HystrixRuntimeException} en caso contrario.
	   */
	  public List<CatalogAgreementResponse> getCatalogAgreementFallback(CatalogAgreementRequest request, Throwable ex) {
	    if (ex instanceof HystrixTimeoutException) {
	      throw new DatabaseTimeoutException();
	    }
	    throw new ExternalResourceException(ex.getMessage(), ex);
	  }

}
