/*
 * Copyright (c) 2023 Kairos DS
 *
 * Licensed under the GNU General Public License, Version 3 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 * https://www.gnu.org/licenses/gpl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.bancoppel.servicing.catalog.agreement.constant;

import lombok.Getter;

import org.springframework.stereotype.Component;

/**
 * 
 * @Descripción: Clase que contiene las constantes del Api de convenios.
 * @Autor: Miguel López
 * @Fecha: Abril 20, 2023
 * @Empresa: Kairos DS
 */
@Component
@Getter
public class CatalogAgreementConstants {
	  /**
	   * Descripcion.
	   */
	  public static final String LOG_CATALOG_AGREEMENT_CONTROLLER = "Post getCatalogAgreement";
	  /**
	   * Constante usada para nombrar el metodo de fallback de negocio.
	   */
	  public static final String AGREEMENT_FALLBACK = "getCatalogAgreementFallback";

	  public static final String PAYMENT_AGREEMENT_BUSINESS_TIME =
	      "BUSINESS CALL - [Obtiene el convenio de pago]";
	  /**
	   * Descripcion de log de tiempo de inicio para la consulta.
	   */
	  public static final String TIME_REPOSITORY_AGREEMENT =
	      "DATABASE CALL - [Obtiene el convenio correspondiente]";
	  /**
	   * Descripcion de log de tiempo de inicio para la consulta.
	   */
	  public static final String TIME_REPOSITORY_CATALOG =
	      "DATABASE CALL - [Obtiene numero de transaccion para pago de servicio]";
	  /**
	   * Descripcion de log de tiempo de inicio para la consulta.
	   */
	  public static final String TIME_REPOSITORY_PARAMETERS =
	      "DATABASE CALL - [Obtiene numero de cuenta para pago de servicio]";
	  /**
	   * Mensaje que indica que el convenio del servicio no fue encontrado.
	   */
	  public static final String MSG_AGREEMENT_NOT_FOUNT = "Convenio del servicio no ha sido encotrado";
	  /**
	   * Mensaje que induca que la cuenta destino del servicio no fue encontrada.
	   */
	  public static final String MSG_DESTINATION_ACCOUNT_NOT_FOUND =
	      "La cuenta destino del servicio no ha sido encontrada";
	  /**
	   * Mensaje que indica que el numero de transacción del servicio no fue encontrado.
	   */
	  public static final String MSG_TRANSACTION_NUMBER_NOT_FOUND =
	      "El numero de transaccion del servicio no ha sido encontrado";

}
